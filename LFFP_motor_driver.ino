//****************************************************************************************
// Large Format Film Processor (LFFP)
// Rotate film drums (e.g. for 8x10) using a NEMA step motor
// by N. Repp, 2024 (https://siebblende.de)
// Prerequisites: TB6600 stepper driver, 1600 steps per rotation configured (OFF, ON, OFF)
// Button with 3 positions: "I" - constant, "O" - stop, "II" - alternating
//****************************************************************************************

#define dirPin 2
#define stepPin 3
#define buttonPin1 4
#define buttonPin2 5
#define rotationsPerDirection 5
int button1;
int button2;

void setup() {
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
  digitalWrite(buttonPin1, HIGH); // Reminder: turn on pullup resistors
  digitalWrite(buttonPin2, HIGH); // Reminder: turn on pullup resistors
  
}

void loop() {
 
  button1 = digitalRead(buttonPin1);
  button2 = digitalRead(buttonPin2);

  if (button1 == 0) {
    constantRotations();
  }  
  else if (button2 == 0) {
    alternatingRotations();
  }
  else {

  }
}

void alternatingRotations(){
  
  //Set first direction
  digitalWrite(dirPin, HIGH);

  for (int i=0; i < rotationsPerDirection * 1600; i++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);

  // Change direction
  digitalWrite(dirPin, LOW);

  for (int i=0; i < rotationsPerDirection * 1600; i++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);
}

void constantRotations(){
  
  digitalWrite(dirPin, HIGH);

  for (int i=0; i < rotationsPerDirection * 1600; i++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

}